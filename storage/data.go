package storage

import (
	"github.com/orcaman/concurrent-map/v2"
)

var data = cmap.New[any]()

func Get() *cmap.ConcurrentMap[string, any] {
	return &data
}
