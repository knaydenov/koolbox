package output

import (
	"io"
	"os"
)

type StdoutTargetHandlerConfig struct{}

func StdoutTargetHandler(config interface{}) (io.Writer, error) {
	return os.Stdout, nil
}
