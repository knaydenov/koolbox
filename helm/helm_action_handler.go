package helm

import (
	"context"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/render"
	helm_loader "helm.sh/helm/v3/pkg/chart/loader"
)

type ActionHandlerConfig struct {
	Path string `doc:"Helm chart path. Templating supported."`
}

func ActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c ActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Path, err = render.Render(c.Path)
	if err != nil {
		return nil, err
	}

	chart, err := helm_loader.Load(c.Path)

	handlerData["chart"] = chart

	return handlerData, nil
}
