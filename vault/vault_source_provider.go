package vault

import (
	"context"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/render"
)

type HttpClientConfig struct {
	InsecureSkipVerify bool `mapstructure:"insecure_skip_verify" default:"false"`
}

type SourceConfig struct {
	Address string
	Auth    struct {
		Type     string
		Username string
		Password string
	}
	Path             string
	Filters          []string
	HttpClientConfig HttpClientConfig `mapstructure:"http_client_config"`
}

func Source(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c SourceConfig
	var returnData = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Address, err = render.Render(c.Address)
	if err != nil {
		return nil, err
	}

	c.Path, err = render.Render(c.Path)
	if err != nil {
		return nil, err
	}

	c.Auth.Username, err = render.Render(c.Auth.Username)
	if err != nil {
		return nil, err
	}

	c.Auth.Password, err = render.Render(c.Auth.Password)
	if err != nil {
		return nil, err
	}

	clientConfig := ClientConfig{
		Address:          c.Address,
		Auth:             c.Auth,
		HttpClientConfig: c.HttpClientConfig,
	}

	client, err := createClient(&clientConfig)
	if err != nil {
		return nil, err
	}

	logger.Debug("msg", "Fetching secret", "path", c.Path)

	secret, err := client.Logical().Read(c.Path)
	if err != nil {
		return nil, err
	}

	if secret != nil {

		secretData := fetchDataFromSecret(secret)
		logger.Debug("msg", "Secrets fetched", "count", len(*secretData))

		for k, v := range *secretData {
			returnData[k] = v
		}
	}

	filteredData, err := helper.FilterMap(&returnData, c.Filters)
	if err != nil {
		return nil, err
	}

	return filteredData, nil
}

func fetchDataFromSecret(s *api.Secret) *map[string]interface{} {
	_, hasDataKey := s.Data["data"]
	if hasDataKey {
		secretData, ok := s.Data["data"].(map[string]interface{})
		if ok {
			return &secretData
		}
	}

	return &map[string]interface{}{}
}
