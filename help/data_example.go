package help

import (
	"gitlab.com/knaydenov/koolbox/data"
	"gitlab.com/knaydenov/koolbox/task"
)

var DataTaskExample = []task.Task{
	{
		Action: "data",
		Config: data.ActionHandlerConfig{
			Src: []data.ActionHandlerSrc{
				{
					Type: "manual",
					Config: data.ManualSourceConfig{
						Data: map[string]string{
							"VAR1": "value",
						},
					},
				},
				{
					Type: "env",
					Config: data.EnvSourceConfig{
						Filters: []string{
							"^SOME_PREFIX_",
							"SOME_SUFFIX$",
						},
					},
				},
			},
			Dst: []data.ActionHandlerDst{
				{
					Type: "dotenv",
					Config: data.DotenvDestinationConfig{
						Dst: "/some/file/path/.env",
						Filters: []string{
							"^SOME_PREFIX_",
						},
						CreateDir: false,
					},
				},
				{
					Type: "env",
					Config: data.EnvDestinationConfig{
						Filters: []string{
							"^SOME_PREFIX_",
						},
						AddPrefix:  "SOME_PREFIX_",
						AddSuffix:  "_SOME_SUFFIX",
						TrimPrefix: "REMOVE_PREFIX_",
						TrimSuffix: "_REMOVE_PREFIX",
					},
				},
				{
					Type: "log",
					Config: data.LogDestinationConfig{
						Debug: true,
						Filters: []string{
							"^SOME_PREFIX_",
						},
					},
				},
			},
		},
	},
}
