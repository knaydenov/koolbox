package output

type Target struct {
	Name              string             `doc:"Output target name (optional)"`
	Type              string             `doc:"Output target type (forward | syslog)"`
	Enabled           bool               `default:"true" doc:"(optional)"`
	EnabledExpression string             `mapstructure:"enabled_expression" default:"" doc:"If set 'enabled' key will be ignored. Templating supported."`
	Filters           []string           `doc:"Regex filters."`
	FilterBuffer      FilterBufferConfig `doc:"Filter buffer configuration"`
	Config            interface{}        `doc:"Output target configuration."`
}

type FilterBufferConfig struct {
	MinInterval string `mapstructure:"min_interval" default:"100us" doc:"Backoff retry min interval."`
	MaxInterval string `mapstructure:"max_interval" default:"1s" doc:"Backoff retry max interval."`
}
