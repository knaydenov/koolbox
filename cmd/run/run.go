package run

import (
	"context"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	_data "gitlab.com/knaydenov/koolbox/data"
	"gitlab.com/knaydenov/koolbox/debug"
	"gitlab.com/knaydenov/koolbox/doc"
	"gitlab.com/knaydenov/koolbox/exec"
	"gitlab.com/knaydenov/koolbox/helm"
	"gitlab.com/knaydenov/koolbox/help"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/localization"
	"gitlab.com/knaydenov/koolbox/net"
	"gitlab.com/knaydenov/koolbox/render"
	"gitlab.com/knaydenov/koolbox/storage"
	"gitlab.com/knaydenov/koolbox/task"
)

type Options struct {
}

var ahr task.ActionHandlerRegistry

func init() {
	var err error
	ahr = task.NewActionHandlerRegistry()

	err = ahr.Register("debug", debug.ActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("render", render.ActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("data", _data.ActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("ping", net.PingActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("nc", net.NcActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("download", net.DownloadActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("exec", exec.ActionHandler)
	cobra.CheckErr(err)

	err = ahr.Register("helm", helm.ActionHandler)
	cobra.CheckErr(err)
}

func NewCmd(options Options) *cobra.Command {

	cmd := &cobra.Command{
		Use:     "run",
		Short:   localization.LocalizeMessage("Run tasks"),
		Example: buildExample(),
		Run: func(cmd *cobra.Command, args []string) {
			options.Run(cmd.Context(), cmd, args)
		},
	}

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {
	var err error
	var tasks []task.Task

	err = mapstructure.Decode(viper.Get("tasks"), &tasks)
	cobra.CheckErr(err)

	for i, t := range tasks {
		t.Name = helper.DefaultString(t.Name, fmt.Sprintf("task_%d", i))
		taskData, err := t.Run(ctx, ahr, storage.Get().Items())
		cobra.CheckErr(err)

		storage.Get().SetIfAbsent("Tasks", make(map[string]any))
		tasksData, _ := storage.Get().Get("Tasks")
		tasksData.(map[string]interface{})[t.Name] = taskData
		storage.Get().Set("Tasks", tasksData)
	}
}

func buildExample() string {
	var err error

	example, err := doc.BuildYaml(help.GetConfig())
	cobra.CheckErr(err)

	return example
}
