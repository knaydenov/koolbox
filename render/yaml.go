package render

import "gopkg.in/yaml.v3"

func fromYaml(data string) interface{} {
	var err error
	var result interface{}

	err = yaml.Unmarshal([]byte(data), &result)
	if err != nil {
		return ""
	}

	return result
}

func toYaml(data interface{}, args ...string) string {
	var err error
	var b []byte

	b, err = yaml.Marshal(data)
	if err != nil {
		return ""
	}

	return string(b)
}
