package helper

func DefaultString(value string, defaultValue string) string {
	if value != "" {
		return value
	}

	return defaultValue
}

// DefaultBool TODO: remove?
func DefaultBool(value bool, defaultValue bool) bool {
	if value != false {
		return value
	}

	return defaultValue
}
