package helper

import (
	"gitlab.com/knaydenov/koolbox/logger"
	"os"
)

func EnsureDirExists(path string, createIfNotExists bool) error {
	var err error

	_, err = os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			if createIfNotExists {
				logger.Debug("msg", "Creating directory", "path", path)
				err = os.MkdirAll(path, os.ModePerm)
				if err != nil {
					return err
				}

				return nil
			} else {
				return err
			}
		}
	}

	return nil
}
