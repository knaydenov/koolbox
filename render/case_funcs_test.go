package render

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestKebab2Camel(t *testing.T) {
	assert.Equal(t, "", kebab2Camel(""))
	assert.Equal(t, "1", kebab2Camel("1"))
	assert.Equal(t, "hello1World", kebab2Camel("Hello-1-World"))
	assert.Equal(t, "hello", kebab2Camel("hello"))
	assert.Equal(t, "helloWorld", kebab2Camel("hello-world"))
	assert.Equal(t, "helloWorld", kebab2Camel("Hello-world"))
	assert.Equal(t, "helloWorld", kebab2Camel("Hello-World"))
	assert.Equal(t, "helloWorld", kebab2Camel("Hello-WorlD"))
	assert.Equal(t, "helloworld", kebab2Camel("HelloWorlD"))
	assert.Equal(t, "helloBigWorld", kebab2Camel("hello-big-world"))
	assert.Equal(t, "item01", kebab2Camel("item-01"))
}

func TestKebab2Snake(t *testing.T) {
	assert.Equal(t, "", kebab2Snake(""))
	assert.Equal(t, "1", kebab2Snake("1"))
	assert.Equal(t, "hello_world", kebab2Snake("hello-world"))
	assert.Equal(t, "hello_world", kebab2Snake("hello-World"))
	assert.Equal(t, "_hello_world", kebab2Snake("-hello-World"))
	assert.Equal(t, "123_333", kebab2Snake("123-333"))
	assert.Equal(t, "123_333", kebab2Snake("123_333"))
	assert.Equal(t, "b123_333a", kebab2Snake("B123_333A"))
}

func TestCamel2Kebab(t *testing.T) {
	assert.Equal(t, "", camel2Kebab(""))
	assert.Equal(t, "1", camel2Kebab("1"))
	assert.Equal(t, "hello-World", camel2Kebab("helloWorld"))
	assert.Equal(t, "Hello---World", camel2Kebab("Hello-World"))
	assert.Equal(t, "--Hello---World", camel2Kebab("-Hello-World"))
	assert.Equal(t, "123---333", camel2Kebab("123-333"))
	assert.Equal(t, "123-_-333", camel2Kebab("123_333"))
	assert.Equal(t, "B-123-_-333-A", camel2Kebab("B123_333A"))
}

func TestCamel2Snake(t *testing.T) {
	assert.Equal(t, "", camel2Snake(""))
	assert.Equal(t, "1", camel2Snake("1"))
	assert.Equal(t, "hello_world", camel2Snake("helloWorld"))
	assert.Equal(t, "hello_-_world", camel2Snake("Hello-World"))
	assert.Equal(t, "-_hello_-_world", camel2Snake("-Hello-World"))
	assert.Equal(t, "123_-_333", camel2Snake("123-333"))
	assert.Equal(t, "123___333", camel2Snake("123_333"))
	assert.Equal(t, "b_123___333_a", camel2Snake("B123_333A"))
}

func TestSnake2Kebab(t *testing.T) {
	assert.Equal(t, "", snake2Kebab(""))
	assert.Equal(t, "1", snake2Kebab("1"))
	assert.Equal(t, "hello-world", snake2Kebab("hello_world"))
	assert.Equal(t, "hello-World", snake2Kebab("hello_World"))
	assert.Equal(t, "-hello-World", snake2Kebab("_hello_World"))
	assert.Equal(t, "123-333", snake2Kebab("123_333"))
	assert.Equal(t, "123-333", snake2Kebab("123_333"))
	assert.Equal(t, "B123-333A", snake2Kebab("B123_333A"))
}

func TestSnake2Camel(t *testing.T) {
	assert.Equal(t, "", snake2Camel(""))
	assert.Equal(t, "1", snake2Camel("1"))
	assert.Equal(t, "hello1World", snake2Camel("Hello_1_World"))
	assert.Equal(t, "hello", snake2Camel("hello"))
	assert.Equal(t, "helloWorld", snake2Camel("hello_world"))
	assert.Equal(t, "helloWorld", snake2Camel("Hello_world"))
	assert.Equal(t, "helloWorld", snake2Camel("Hello_World"))
	assert.Equal(t, "helloWorld", snake2Camel("Hello_WorlD"))
	assert.Equal(t, "helloworld", snake2Camel("HelloWorlD"))
	assert.Equal(t, "helloBigWorld", snake2Camel("hello_big_world"))
	assert.Equal(t, "item-01", snake2Camel("item-01"))
}
