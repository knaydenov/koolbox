package net

import (
	"context"
	"fmt"
)

type HttpDataMarshaler func(ctx context.Context, config interface{}, body []byte) (map[string]interface{}, error)

type HttpDataMarshalerRegistry interface {
	Register(name string, httpDataMarshaler HttpDataMarshaler) error
	Get(name string) (HttpDataMarshaler, error)
}

type httpDataMarshalerRegistry struct {
	httpDataMarshalers map[string]HttpDataMarshaler
}

func (r *httpDataMarshalerRegistry) Register(name string, httpDataMarshaler HttpDataMarshaler) error {
	r.httpDataMarshalers[name] = httpDataMarshaler

	return nil
}

func (r *httpDataMarshalerRegistry) Get(name string) (HttpDataMarshaler, error) {
	if h, ok := r.httpDataMarshalers[name]; ok {
		return h, nil
	}

	return nil, fmt.Errorf("http data marshaler \"%s\" not found", name)
}

func NewHttpDataMarshalerRegistry() HttpDataMarshalerRegistry {
	return &httpDataMarshalerRegistry{
		httpDataMarshalers: make(map[string]HttpDataMarshaler),
	}
}
