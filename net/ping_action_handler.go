package net

import (
	"context"
	"github.com/go-ping/ping"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/render"
)

type PingActionHandlerConfig struct {
	Host       string `doc:"Target hostname. Templating supported."`
	Count      int    `default:"5" doc:"Ping attempts."`
	Privileged bool   `default:"true" doc:"Should privileged flag be set?"`
}

func PingActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c PingActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Host, err = render.Render(c.Host)
	if err != nil {
		return nil, err
	}

	pinger, err := ping.NewPinger(c.Host)
	if err != nil {
		return nil, err
	}

	pinger.Count = c.Count

	err = pinger.Run() // Blocks until finished.
	if err != nil {
		return nil, err
	}

	pinger.SetPrivileged(c.Privileged)

	stats := pinger.Statistics()

	logger.Info(
		"msg",
		"Ping",
		"Addr",
		stats.Addr,
		"MaxRtt",
		stats.MaxRtt,
		"MinRtt",
		stats.MinRtt,
		"AvgRtt",
		stats.AvgRtt,
		"PacketsRecv",
		stats.PacketsRecv,
		"PacketsSent",
		stats.PacketsSent,
		"PacketLoss",
		stats.PacketLoss,
	)

	handlerData = map[string]interface{}{
		"Addr":        stats.Addr,
		"MaxRtt":      stats.MaxRtt,
		"MinRtt":      stats.MinRtt,
		"AvgRtt":      stats.AvgRtt,
		"PacketsRecv": stats.PacketsRecv,
		"PacketsSent": stats.PacketsSent,
		"PacketLoss":  stats.PacketLoss,
	}

	return handlerData, nil
}
