package fluent

import (
	"bufio"
	"github.com/IBM/fluent-forward-go/fluent/client"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/render"
	"io"
	"time"
)

type TargetHandlerConfig struct {
	Enabled    bool   `default:"false" doc:"Deprecated"`
	Network    string `default:"tcp" doc:"Network protocol (tcp|udp). Templating supported."`
	Address    string `default:"localhost:5170" doc:"Destination server address. Templating supported."`
	Timeout    string `default:"0s" doc:"Dial timeout. Templating supported."`
	MaxRetries int    `default:"3" doc:"Maximum retries"`
}

type ForwardWriter struct {
	io.Writer
	c       *client.Client
	buffer  *io.PipeWriter
	Address string
	Timeout string
}

type ForwardWriterConfig struct {
	Address    string
	Timeout    string
	MaxRetries int
}

func NewWriter(config ForwardWriterConfig) (*ForwardWriter, error) {
	var err error
	var fw ForwardWriter
	var reader *io.PipeReader

	reader, fw.buffer = io.Pipe()

	timeout, err := time.ParseDuration(config.Timeout)
	if err != nil {
		return nil, err
	}

	fw.c = client.New(client.ConnectionOptions{
		Factory: &client.ConnFactory{
			Address: config.Address,
			Timeout: timeout,
		},
	})

	err = fw.c.Connect()
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(reader)

	go func() {
		for scanner.Scan() {
			fw.Send(append(scanner.Bytes(), '\n'), config.MaxRetries)
		}
	}()

	return &fw, nil
}

func (w *ForwardWriter) Send(p []byte, retries int) {
	var err error

	err = w.c.SendRaw(p)
	if err != nil && retries > 0 {
		err = w.c.Reconnect()
		w.Send(p, retries-1)
	}
}

func (w *ForwardWriter) Write(p []byte) (n int, err error) {
	return w.buffer.Write(p)
}

func TargetHandler(config interface{}) (io.Writer, error) {
	var err error
	var c TargetHandlerConfig

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Network, err = render.Render(c.Network)
	if err != nil {
		return nil, err
	}

	c.Timeout, err = render.Render(c.Timeout)
	if err != nil {
		return nil, err
	}

	c.Address, err = render.Render(c.Address)
	if err != nil {
		return nil, err
	}

	return NewWriter(ForwardWriterConfig{
		Address:    c.Address,
		Timeout:    c.Timeout,
		MaxRetries: c.MaxRetries,
	})
}
