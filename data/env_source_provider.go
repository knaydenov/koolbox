package data

import (
	"context"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"os"
	"strings"
)

type EnvSourceConfig struct {
	Filters []string `doc:"Regex filters for environment variable name."`
}

func EnvSource(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c EnvSourceConfig
	var envData = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	for _, pair := range os.Environ() {
		p := strings.Split(pair, "=")

		if len(p) > 1 {
			envData[p[0]] = p[1]
		} else {
			envData[p[0]] = ""
		}
	}

	filteredData, err := helper.FilterMap(&envData, c.Filters)
	if err != nil {
		return nil, err
	}

	return filteredData, nil
}
