package task

import (
	"context"
	"gitlab.com/knaydenov/koolbox/logger"
)

type Task struct {
	Name    string      `doc:"Action name. If not set then task number will be used."`
	Action  string      `doc:"Action type."`
	Config  interface{} `doc:"Action configuration."`
	OnError string      `mapstructure:"on_error" default:"fail" doc:"On error behaviour (fail|skip)."`
}

type Data struct {
	Tasks []map[string]interface{}
}

func (t *Task) Run(ctx context.Context, ahr ActionHandlerRegistry, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var handler ActionHandler

	logger.Info("msg", "Running task", "name", t.Name)

	handler, err = ahr.Get(t.Action)
	if err != nil {
		return nil, err
	}

	taskData, err := handler(ctx, t.Config, data)
	if err != nil {
		switch t.OnError {
		case "skip":
			errorData := make(map[string]interface{})
			errorData["Error"] = err
			return errorData, nil
		case "fail":
			fallthrough
		default:
			return nil, err
		}
	}

	return taskData, nil
}
