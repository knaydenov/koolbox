package syslog

import (
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/render"
	"io"
)

type TargetHandlerConfig struct {
	Format    string `default:"rfc3164" doc:"Syslog message format (rfc3164|rfc5424). Templating supported."`
	Version   uint64 `default:"1" doc:"Message version number (rfc5424)."`
	MessageID string `mapstructure:"message_id" default:"" doc:"Syslog message id (rfc5424). Templating supported."`
	ProcessID string `mapstructure:"process_id" default:"" doc:"Syslog process id (rfc5424). If empty PID of process will be used. Templating supported."`
	Network   string `default:"udp" doc:"Network protocol (tcp|udp). Templating supported."`
	Address   string `default:"localhost:514" doc:"Syslog server address (localhost:514). Templating supported."`
	Severity  string `default:"emerg" doc:"Severity level (emerg|alert|crit|err|warning|notice|info|debug). Templating supported."`
	Facility  string `default:"local0" doc:"Facility (local0|local1|local2|local3|local4|local5|local6|local7). Templating supported."`
	Tag       string `default:"default" doc:"Tag (some_tag). Templating supported."`
}

func TargetHandler(config interface{}) (io.Writer, error) {
	var err error
	var c TargetHandlerConfig

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Format, err = render.Render(c.Format)
	if err != nil {
		return nil, err
	}

	c.ProcessID, err = render.Render(c.ProcessID)
	if err != nil {
		return nil, err
	}

	c.MessageID, err = render.Render(c.MessageID)
	if err != nil {
		return nil, err
	}

	c.Facility, err = render.Render(c.Facility)
	if err != nil {
		return nil, err
	}

	c.Severity, err = render.Render(c.Severity)
	if err != nil {
		return nil, err
	}

	priority, err := getPriority(c.Facility, c.Severity)
	if err != nil {
		return nil, err
	}

	c.Address, err = render.Render(c.Address)
	if err != nil {
		return nil, err
	}

	c.Network, err = render.Render(c.Network)
	if err != nil {
		return nil, err
	}

	c.Tag, err = render.Render(c.Tag)
	if err != nil {
		return nil, err
	}

	logger.Debug("msg", "Creating syslog writer", "address", c.Address, "network", c.Network, "facility", c.Facility, "severity", c.Severity, "tag", c.Tag, "procid", c.ProcessID, "msgid", c.MessageID, "format", c.Format)

	return NewWriter(c.Network, c.Address, uint16(c.Version), priority, c.Tag, c.ProcessID, c.MessageID, c.Format)
}
