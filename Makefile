.PHONY: clean distclean deps test build build-docs pack

all: clean distclean deps test build build-docs pack

clean:
	rm -rf bin

distclean:
	rm -rf bin dist docs

deps:
	@go get -v -d ./...

test:
	go test ./...

setup:
ifeq ($(ARCH), noarch)
CGO_ENABLED := 0
GOOS :=
GOARCH :=
else
CGO_ENABLED := 1
GOOS := $(OS)
GOARCH := $(ARCH)
endif

build:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) go build -ldflags="-X 'gitlab.com/knaydenov/koolbox/version.Version=${KOOLBOX_VERSION}'" -o bin/koolbox koolbox.go

build-docs:
	LANG=en go run doc/cmd/generate.go bin/docs
	LANG=ru go run doc/cmd/generate.go bin/docs

pack:
	mkdir -p dist
	cd bin && tar cvfz ../dist/koolbox-$(OS)-$(ARCH).tar.gz *
