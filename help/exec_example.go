package help

import (
	"gitlab.com/knaydenov/koolbox/exec"
	"gitlab.com/knaydenov/koolbox/task"
)

var ExecTaskExample = []task.Task{
	{
		Action: "exec",
		Config: exec.ActionHandlerConfig{
			Cmd:         "echo",
			Args:        []string{"-n", "hello"},
			PrintOutput: true,
		},
	},
}
