package data

import (
	"context"
	"fmt"
)

type SourceProvider func(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error)

type SourceProviderRegistry interface {
	Register(name string, sourceProvider SourceProvider) error
	Get(name string) (SourceProvider, error)
}

type sourceProviderRegistry struct {
	sourceProviders map[string]SourceProvider
}

func (r *sourceProviderRegistry) Register(name string, sourceProvider SourceProvider) error {
	r.sourceProviders[name] = sourceProvider

	return nil
}

func (r *sourceProviderRegistry) Get(name string) (SourceProvider, error) {
	if h, ok := r.sourceProviders[name]; ok {
		return h, nil
	}

	return nil, fmt.Errorf("source \"%s\" not found", name)
}

func NewSourceProviderRegistry() SourceProviderRegistry {
	return &sourceProviderRegistry{
		sourceProviders: make(map[string]SourceProvider),
	}
}

