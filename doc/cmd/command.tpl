# {{ .Name }}

{{ if .Synopsis -}}
## {{ "Synopsis" | localize }}

{{ .Synopsis }}
{{- end }}
{{ if .Description -}}
## {{ "Description" | localize }}

{{ .Description }}
{{- end }}
{{ if .Usage -}}
## {{ "Usage" | localize }}

```bash
{{ .Usage }}
```
{{- end }}
{{ if .Example -}}
## {{ "Examples" | localize }}

```
{{ .Example }}
```
{{- end }}
{{ if gt (.Options | len) 0 -}}
## {{ "Flags" | localize }}

```
{{- range .Options }}
{{ if .Shorthand }}-{{ .Shorthand }}, {{ end }}--{{ .Name }} {{ .Usage }}{{ if .DefaultValue }} ({{ "Default" | localize }} "{{ .DefaultValue }}"){{ end }}
{{- end }}
```
{{- end }}
{{ if gt (.SeeAlso | len) 0 -}}
## {{ "See also" | localize }}
{{- range .SeeAlso }}
* [{{ . }}]({{ ((first (. | splitList " - ")) | splitList " ") | join "_" }}.md)
{{- end }}
{{- end }}
{{/*koolbox completion - Generate the autocompletion script for the specified shell*/}}