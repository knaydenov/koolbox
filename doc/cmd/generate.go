package main

import (
	"embed"
	"fmt"
	"github.com/Masterminds/sprig/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"gitlab.com/knaydenov/koolbox/cmd"
	"gitlab.com/knaydenov/koolbox/render"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
	"text/template"
)

//go:embed *.tpl
var LocaleFS embed.FS

type commandDoc struct {
	Name        string `yaml:"name"`
	Synopsis    string `yaml:"synopsis"`
	Description string `yaml:"description"`
	Usage       string `yaml:"usage"`
	Example     string `yaml:"example"`
	Options     []struct {
		Name         string `yaml:"name"`
		Shorthand    string `yaml:"shorthand"`
		DefaultValue string `yaml:"default_value"`
		Usage        string `yaml:"usage"`
	} `yaml:"options"`
	SeeAlso []string `yaml:"see_also"`
}

func main() {
	var err error

	if len(os.Args) < 2 {
		return
	}

	c := cmd.NewCmd(cmd.Options{})
	c.InitDefaultCompletionCmd()
	c.InitDefaultHelpCmd()

	var y = make(map[string]string)
	err = genYamlTree(c, y)
	if err != nil {
		panic(err)
	}

	outputDir := fmt.Sprintf("%s/%s", os.Args[1], os.Getenv("LANG"))

	err = os.MkdirAll(outputDir, os.ModePerm)
	if err != nil {
		panic(err)
	}

	for name, value := range y {
		err = writeFile(renderMarkdown(value), fmt.Sprintf("%s/%s.md", outputDir, name))
		if err != nil {
			panic(err)
		}
	}
}

func writeFile(content string, path string) error {
	var err error
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(content)
	if err != nil {
		return err
	}

	return nil
}

func renderMarkdown(y string) string {
	var err error
	var t commandDoc
	buffer := new(strings.Builder)

	err = yaml.Unmarshal([]byte(y), &t)
	if err != nil {
		panic(err)
	}

	var tmplFile = "command.tpl"
	tmpl, err := template.New(tmplFile).Funcs(render.GenericFuncMap()).Funcs(sprig.GenericFuncMap()).ParseFS(LocaleFS, tmplFile)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(buffer, t)
	if err != nil {
		panic(err)
	}

	return buffer.String()
}

func genYamlTree(command *cobra.Command, y map[string]string) error {
	var err error

	buffer := new(strings.Builder)
	err = doc.GenYaml(command, buffer)
	if err != nil {
		return err
	}

	y[getCommandName(command)] = buffer.String()

	for _, child := range command.Commands() {
		err = genYamlTree(child, y)
		if err != nil {
			return err
		}
	}

	return nil
}

func getCommandName(command *cobra.Command) string {
	var parts []string

	if command.HasParent() {
		parts = append(parts, getCommandName(command.Parent()))
	}

	parts = append(parts, command.Name())

	return strings.Join(parts, "_")
}
