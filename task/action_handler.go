package task

import (
	"context"
	"fmt"
)

type ActionHandler func(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error)

type ActionHandlerRegistry interface {
	Register(name string, actionHandler ActionHandler) error
	Get(name string) (ActionHandler, error)
}

type actionHandlerRegistry struct {
	actionHandlers map[string]ActionHandler
}

func (r *actionHandlerRegistry) Register(name string, actionHandler ActionHandler) error {
	r.actionHandlers[name] = actionHandler

	return nil
}

func (r *actionHandlerRegistry) Get(name string) (ActionHandler, error) {
	if h, ok := r.actionHandlers[name]; ok {
		return h, nil
	}

	return nil, fmt.Errorf("action handler \"%s\" not found", name)
}

func NewActionHandlerRegistry() ActionHandlerRegistry {
	return &actionHandlerRegistry{
		actionHandlers: make(map[string]ActionHandler),
	}
}
