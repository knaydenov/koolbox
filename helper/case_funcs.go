package helper

import (
	"github.com/fatih/camelcase"
	"strings"
)

func Kebab2Camel(s string) string {
	var tokens []string
	for i, token := range strings.Split(s, "-") {
		token = strings.ToLower(token)

		if i != 0 {
			token = strings.Title(token)
		}

		tokens = append(tokens, token)
	}

	return strings.Join(tokens, "")
}

func Kebab2Snake(s string) string {
	return strings.ToLower(strings.Join(strings.Split(s, "-"), "_"))
}

func Camel2Kebab(s string) string {
	var tokens []string

	for _, token := range camelcase.Split(s) {
		tokens = append(tokens, token)
	}

	return strings.Join(tokens, "-")
}

func Camel2Snake(s string) string {
	var tokens []string

	for _, token := range camelcase.Split(s) {
		tokens = append(tokens, strings.ToLower(token))
	}

	return strings.Join(tokens, "_")
}

func Snake2Kebab(s string) string {
	var tokens []string
	for _, token := range strings.Split(s, "_") {
		tokens = append(tokens, token)
	}

	return strings.Join(tokens, "-")
}

func Snake2Camel(s string) string {
	var tokens []string
	for i, token := range strings.Split(s, "_") {
		token = strings.ToLower(token)

		if i != 0 {
			token = strings.Title(token)
		}

		tokens = append(tokens, token)
	}

	return strings.Join(tokens, "")
}
