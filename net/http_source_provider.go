package net

import (
	"context"
	"crypto/tls"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	"gitlab.com/knaydenov/koolbox/render"
	"io/ioutil"
	"net/http"
)

var hdmr HttpDataMarshalerRegistry

type HttpClientConfig struct {
	InsecureSkipVerify bool `mapstructure:"insecure_skip_verify" default:"false" doc:"Should TLS verification be skipped?"`
}

type HttpSourceConfig struct {
	Src          string           `doc:"Source URL. Templating supported."`
	ClientConfig HttpClientConfig `mapstructure:"client_config" doc:"HTTP client configuration."`
	Marshaler    struct {
		Name   string                 `default:"json" doc:"Data marshaller (json)."`
		Config map[string]interface{} `doc:"Data marshaller configuration options."`
	}
}

func init() {
	var err error
	hdmr = NewHttpDataMarshalerRegistry()

	err = hdmr.Register("json", JsonHttpDataMarshaler)
	cobra.CheckErr(err)
}

func HttpSource(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c HttpSourceConfig
	var marshaler HttpDataMarshaler
	var d = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Src, err = render.Render(c.Src)
	if err != nil {
		return nil, err
	}

	marshaler, err = hdmr.Get(c.Marshaler.Name)
	if err != nil {
		return nil, err
	}

	client, err := createHttpClient(c.ClientConfig)
	response, err := client.Get(c.Src)
	if err != nil {
		return nil, err
	}

	d["status_code"] = response.StatusCode

	d["body"], err = ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	d["data"], err = marshaler(ctx, c.Marshaler.Config, d["body"].([]byte))
	if err != nil {
		return nil, err
	}

	return d, nil
}

func createHttpClient(config HttpClientConfig) (*http.Client, error) {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: config.InsecureSkipVerify},
	}

	return &http.Client{Transport: transport}, nil
}
