package version

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/knaydenov/koolbox/localization"
	"gitlab.com/knaydenov/koolbox/version"
	"runtime"
)

type Options struct {
}

func NewCmd(o Options) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "version",
		Short: localization.LocalizeMessage("Get version"),
		Run: func(cmd *cobra.Command, args []string) {
			o.Run(cmd.Context(), cmd, args)
		},
	}

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {
	var err error

	fmt.Println(fmt.Sprintf("%s-%s.%s", version.Version, runtime.GOOS, runtime.GOARCH))
	cobra.CheckErr(err)
}
