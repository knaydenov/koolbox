package data

import (
	"context"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/logger"
)

type LogDestinationConfig struct {
	Filters []string `doc:"Regex filters for variable name."`
	Debug   bool     `mapstructure:"debug" default:"false" doc:"Enable debug output."`
}

func LogDestination(ctx context.Context, config interface{}, data map[string]interface{}) error {
	var err error
	var c LogDestinationConfig

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return err
	}

	filteredData, err := helper.FilterMap(&data, c.Filters)
	if err != nil {
		return err
	}

	for k, v := range filteredData {
		logData := []interface{}{"msg", "Data", "key", k, "value", v}

		if c.Debug {
			logger.Debug(logData...)

		} else {
			logger.Info(logData...)

		}
	}

	return nil
}
