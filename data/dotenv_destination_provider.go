package data

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/render"
	"path"
)

type DotenvDestinationConfig struct {
	Dst       string   `doc:"Dotenv file path. Templating supported."`
	Filters   []string `doc:"Regex filters."`
	CreateDir bool     `mapstructure:"create_dir" default:"true" doc:"Should the target directory be created?"`
}

func DotenvDestination(ctx context.Context, config interface{}, data map[string]interface{}) error {
	var err error
	var c DotenvDestinationConfig

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return err
	}

	filteredData, err := helper.FilterMap(&data, c.Filters)
	if err != nil {
		return err
	}

	c.Dst, err = render.Render(c.Dst)
	if err != nil {
		return err
	}

	var returnData = make(map[string]string)

	for k, v := range filteredData {
		returnData[k] = fmt.Sprintf("%v", v)
	}

	outputDir := path.Dir(c.Dst)

	err = helper.EnsureDirExists(outputDir, c.CreateDir)
	if err != nil {
		return err
	}

	err = godotenv.Write(returnData, c.Dst)
	if err != nil {
		return err
	}

	return nil
}
