package debug

import (
	"context"
	"encoding/json"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/render"
	"os"
	"path"
)

type Config struct {
	Dst       string `default:"/dev/stdout" doc:"Output destination."`
	CreateDir bool   `mapstructure:"create_dir" default:"true" doc:"Should the target directory be created?"`
}

func ActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c Config
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	var dst string

	dst, err = render.Render(c.Dst)
	if err != nil {
		return nil, err
	}

	outputDir := path.Dir(dst)

	err = helper.EnsureDirExists(outputDir, c.CreateDir)
	if err != nil {
		return nil, err
	}

	var f *os.File

	f, err = os.Create(dst)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	_, err = f.Write(jsonData)
	if err != nil {
		return nil, err
	}

	return handlerData, nil
}
