package render

import (
	"gitlab.com/knaydenov/koolbox/helper"
)

func kebab2Camel(s string) string {
	return helper.Kebab2Camel(s)
}

func kebab2Snake(s string) string {
	return helper.Kebab2Snake(s)
}

func camel2Kebab(s string) string {
	return helper.Camel2Kebab(s)
}

func camel2Snake(s string) string {
	return helper.Camel2Snake(s)
}

func snake2Kebab(s string) string {
	return helper.Snake2Kebab(s)
}

func snake2Camel(s string) string {
	return helper.Snake2Camel(s)
}
