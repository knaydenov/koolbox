package output

import (
	"bytes"
	"fmt"
	"github.com/cenkalti/backoff/v4"
	"io"
	"regexp"
	"sync"
	"time"
)

type filteringWriter struct {
	inputBuffer *bytes.Buffer
	mutex       sync.Mutex
}

func (f *filteringWriter) Write(p []byte) (n int, err error) {
	f.mutex.Lock()
	defer f.mutex.Unlock()
	f.inputBuffer.Write(p)

	return len(p), err
}

func NewFilteringWriter(outputWriter io.Writer, filters []string, minInterval time.Duration, maxInterval time.Duration) *filteringWriter {
	var fs []*regexp.Regexp

	for _, filter := range filters {
		r, err := regexp.Compile(filter)
		if err != nil {
			continue
		}

		fs = append(fs, r)
	}

	fw := filteringWriter{
		inputBuffer: bytes.NewBuffer(make([]byte, 0)),
	}

	go func() {
		outputBuffer := bytes.NewBuffer(make([]byte, 0))
		sendBuffer := bytes.NewBuffer(make([]byte, 0))

		for {
			bo := backoff.NewExponentialBackOff()
			bo.MaxInterval = maxInterval
			bo.InitialInterval = minInterval

			backoff.Retry(makeTryProcessBuffer(&fw.mutex, fs, fw.inputBuffer, outputBuffer, sendBuffer, outputWriter), bo)
		}
	}()

	return &fw
}

func makeTryProcessBuffer(mutex *sync.Mutex, fs []*regexp.Regexp, inputBuffer *bytes.Buffer, outputBuffer *bytes.Buffer, sendBuffer *bytes.Buffer, outputWriter io.Writer) func() error {
	return func() error {
		if mutex.TryLock() {
			n, _ := outputBuffer.ReadFrom(inputBuffer)
			inputBuffer.Reset()
			mutex.Unlock()

			if n == 0 {
				return fmt.Errorf("buffer too small")
			}

			for {
				bs, err := outputBuffer.ReadBytes('\n')
				sendBuffer.Write(bs)

				if err == io.EOF {
					break
				}

				for _, f := range fs {
					if f.Match(sendBuffer.Bytes()) {
						outputWriter.Write(sendBuffer.Bytes())
					}
				}

				sendBuffer.Reset()
			}
		}

		return nil
	}
}
