package render

import (
	"gitlab.com/knaydenov/koolbox/helper"
)

func filterMap(filter string, data map[string]interface{}) (map[string]interface{}, error) {
	return helper.FilterMap(&data, []string{filter})
}

func addPrefixMap(prefix string, data map[string]interface{}) (map[string]interface{}, error) {
	return helper.AddPrefixMap(&data, prefix)
}

func addSuffixMap(suffix string, data map[string]interface{}) (map[string]interface{}, error) {
	return helper.AddSuffixMap(&data, suffix)
}

func trimPrefixMap(prefix string, data map[string]interface{}) (map[string]interface{}, error) {
	return helper.TrimPrefixMap(&data, prefix)
}

func trimSuffixMap(suffix string, data map[string]interface{}) (map[string]interface{}, error) {
	return helper.TrimSuffixMap(&data, suffix)
}
