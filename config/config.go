package config

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

type Options struct {
	Path string
}

func Configure(options Options) {
	var err error

	if options.Path != "" {
		useConfigurationFile(options.Path)
	} else {
		useDefaultConfigurationFile(".koolbox")
	}

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	cobra.CheckErr(err)
}

func useConfigurationFile(path string) {
	viper.SetConfigFile(path)
	viper.SetConfigType("yaml")
}

func useDefaultConfigurationFile(fileName string) {
	var err error

	home, err := os.UserHomeDir()
	cobra.CheckErr(err)

	workingDir, err := os.Getwd()
	cobra.CheckErr(err)

	viper.AddConfigPath(home)
	viper.AddConfigPath(workingDir)
	viper.SetConfigType("yaml")
	viper.SetConfigName(fileName)
}
