package helper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestKebab2Camel(t *testing.T) {
	assert.Equal(t, "", Kebab2Camel(""))
	assert.Equal(t, "1", Kebab2Camel("1"))
	assert.Equal(t, "hello1World", Kebab2Camel("Hello-1-World"))
	assert.Equal(t, "hello", Kebab2Camel("hello"))
	assert.Equal(t, "helloWorld", Kebab2Camel("hello-world"))
	assert.Equal(t, "helloWorld", Kebab2Camel("Hello-world"))
	assert.Equal(t, "helloWorld", Kebab2Camel("Hello-World"))
	assert.Equal(t, "helloWorld", Kebab2Camel("Hello-WorlD"))
	assert.Equal(t, "helloworld", Kebab2Camel("HelloWorlD"))
	assert.Equal(t, "helloBigWorld", Kebab2Camel("hello-big-world"))
	assert.Equal(t, "item01", Kebab2Camel("item-01"))
}

func TestKebab2Snake(t *testing.T) {
	assert.Equal(t, "", Kebab2Snake(""))
	assert.Equal(t, "1", Kebab2Snake("1"))
	assert.Equal(t, "hello_world", Kebab2Snake("hello-world"))
	assert.Equal(t, "hello_world", Kebab2Snake("hello-World"))
	assert.Equal(t, "_hello_world", Kebab2Snake("-hello-World"))
	assert.Equal(t, "123_333", Kebab2Snake("123-333"))
	assert.Equal(t, "123_333", Kebab2Snake("123_333"))
	assert.Equal(t, "b123_333a", Kebab2Snake("B123_333A"))
}

func TestCamel2Kebab(t *testing.T) {
	assert.Equal(t, "", Camel2Kebab(""))
	assert.Equal(t, "1", Camel2Kebab("1"))
	assert.Equal(t, "hello-World", Camel2Kebab("helloWorld"))
	assert.Equal(t, "Hello---World", Camel2Kebab("Hello-World"))
	assert.Equal(t, "--Hello---World", Camel2Kebab("-Hello-World"))
	assert.Equal(t, "123---333", Camel2Kebab("123-333"))
	assert.Equal(t, "123-_-333", Camel2Kebab("123_333"))
	assert.Equal(t, "B-123-_-333-A", Camel2Kebab("B123_333A"))
}

func TestCamel2Snake(t *testing.T) {
	assert.Equal(t, "", Camel2Snake(""))
	assert.Equal(t, "1", Camel2Snake("1"))
	assert.Equal(t, "hello_world", Camel2Snake("helloWorld"))
	assert.Equal(t, "hello_-_world", Camel2Snake("Hello-World"))
	assert.Equal(t, "-_hello_-_world", Camel2Snake("-Hello-World"))
	assert.Equal(t, "123_-_333", Camel2Snake("123-333"))
	assert.Equal(t, "123___333", Camel2Snake("123_333"))
	assert.Equal(t, "b_123___333_a", Camel2Snake("B123_333A"))
}

func TestSnake2Kebab(t *testing.T) {
	assert.Equal(t, "", Snake2Kebab(""))
	assert.Equal(t, "1", Snake2Kebab("1"))
	assert.Equal(t, "hello-world", Snake2Kebab("hello_world"))
	assert.Equal(t, "hello-World", Snake2Kebab("hello_World"))
	assert.Equal(t, "-hello-World", Snake2Kebab("_hello_World"))
	assert.Equal(t, "123-333", Snake2Kebab("123_333"))
	assert.Equal(t, "123-333", Snake2Kebab("123_333"))
	assert.Equal(t, "B123-333A", Snake2Kebab("B123_333A"))
}

func TestSnake2Camel(t *testing.T) {
	assert.Equal(t, "", Snake2Camel(""))
	assert.Equal(t, "1", Snake2Camel("1"))
	assert.Equal(t, "hello1World", Snake2Camel("Hello_1_World"))
	assert.Equal(t, "hello", Snake2Camel("hello"))
	assert.Equal(t, "helloWorld", Snake2Camel("hello_world"))
	assert.Equal(t, "helloWorld", Snake2Camel("Hello_world"))
	assert.Equal(t, "helloWorld", Snake2Camel("Hello_World"))
	assert.Equal(t, "helloWorld", Snake2Camel("Hello_WorlD"))
	assert.Equal(t, "helloworld", Snake2Camel("HelloWorlD"))
	assert.Equal(t, "helloBigWorld", Snake2Camel("hello_big_world"))
	assert.Equal(t, "item-01", Snake2Camel("item-01"))
}
