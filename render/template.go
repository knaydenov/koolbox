package render

import (
	"bytes"
	"github.com/Masterminds/sprig/v3"
	"gitlab.com/knaydenov/koolbox/storage"
	"text/template"
)

func Render(src string) (string, error) {
	var err error
	var buffer bytes.Buffer

	t := template.New("t")

	t = t.Funcs(sprig.TxtFuncMap()).Funcs(TxtFuncMap())
	if err != nil {
		return "", err
	}

	t, err = t.Parse(src)
	if err != nil {
		return "", err
	}

	err = t.Execute(&buffer, storage.Get().Items())
	if err != nil {
		return "", err
	}

	return buffer.String(), nil
}
