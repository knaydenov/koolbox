package data

import (
	"context"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	"gitlab.com/knaydenov/koolbox/kubernetes"
	"gitlab.com/knaydenov/koolbox/net"
	"gitlab.com/knaydenov/koolbox/vault"
)

var spr SourceProviderRegistry
var dpr DestinationProviderRegistry

type ActionHandlerSrc struct {
	Type   string      `doc:"Source type (manual|env|k8s|vault|http)."`
	Config interface{} `doc:"Source configuration."`
}

type ActionHandlerDst struct {
	Type   string      `doc:"Destination type (env|dotenv|log)."`
	Config interface{} `doc:"Destination configuration."`
}

type ActionHandlerConfig struct {
	Src []ActionHandlerSrc
	Dst []ActionHandlerDst
}

func init() {
	var err error
	spr = NewSourceProviderRegistry()
	dpr = NewDestinationProviderRegistry()

	err = spr.Register("manual", ManualSource)
	cobra.CheckErr(err)

	err = spr.Register("env", EnvSource)
	cobra.CheckErr(err)

	err = spr.Register("k8s", kubernetes.KubernetesSource)
	cobra.CheckErr(err)

	err = spr.Register("vault", vault.Source)
	cobra.CheckErr(err)

	err = spr.Register("http", net.HttpSource)
	cobra.CheckErr(err)

	err = dpr.Register("env", EnvDestination)
	cobra.CheckErr(err)

	err = dpr.Register("dotenv", DotenvDestination)
	cobra.CheckErr(err)

	err = dpr.Register("log", LogDestination)
	cobra.CheckErr(err)
}

func ActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c ActionHandlerConfig
	var returnData = make(map[string]interface{})
	var handlerData = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	for _, src := range c.Src {
		sp, err := spr.Get(src.Type)
		if err != nil {
			return nil, err
		}

		sourceData, err := sp(ctx, src.Config, data)
		if err != nil {
			return nil, err
		}

		for k, v := range sourceData {
			returnData[k] = v
		}
	}

	for _, dst := range c.Dst {
		dp, err := dpr.Get(dst.Type)
		if err != nil {
			return nil, err
		}

		err = dp(ctx, dst.Config, returnData)
		if err != nil {
			return nil, err
		}
	}

	for k, v := range returnData {
		handlerData[k] = v
	}

	return handlerData, nil
}
