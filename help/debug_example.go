package help

import (
	"gitlab.com/knaydenov/koolbox/debug"
	"gitlab.com/knaydenov/koolbox/task"
)

var DebugTaskExample = []task.Task{
	{
		Name:   "debug",
		Action: "debug",
		Config: debug.Config{
			Dst:       "/dev/stdout",
			CreateDir: false,
		},
	},
}
