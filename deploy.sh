#/usr/bin/env bash

tree dist

publish_binaries() {
  echo "Publishing binaries"

  FILES=$(find dist -type f -exec basename {} \;)

  for FILE in $FILES
  do
    OS=$(echo -n $FILE | sed -E 's/koolbox-(.*)-(.*)\.tar\.gz/\1/g')
    ARCH=$(echo -n $FILE | sed -E 's/koolbox-(.*)-(.*)\.tar\.gz/\2/g')

    echo "Publishing koolbox-${OS}-${ARCH}"
    aws --endpoint-url=https://storage.yandexcloud.net/ s3 cp "dist/koolbox-${OS}-${ARCH}.tar.gz" "s3://$S3_BUCKET_NAME/koolbox/${KOOLBOX_VERSION}/koolbox-${OS}-${ARCH}.tar.gz"
  done
}

publish_binaries
