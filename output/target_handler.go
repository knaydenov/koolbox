package output

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/knaydenov/koolbox/fluent"
	"gitlab.com/knaydenov/koolbox/syslog"
	"io"
)

type TargetHandler func(config interface{}) (io.Writer, error)

func init() {
	var err error
	thr = NewTargetHandlerRegistry()

	err = thr.Register("stdout", StdoutTargetHandler)
	cobra.CheckErr(err)

	err = thr.Register("forward", fluent.TargetHandler)
	cobra.CheckErr(err)

	err = thr.Register("syslog", syslog.TargetHandler)
	cobra.CheckErr(err)
}

type TargetHandlerRegistry interface {
	Register(name string, actionHandler TargetHandler) error
	Get(name string) (TargetHandler, error)
}

type targetHandlerRegistry struct {
	targetHandlers map[string]TargetHandler
}

func (r *targetHandlerRegistry) Register(name string, targetHandler TargetHandler) error {
	r.targetHandlers[name] = targetHandler

	return nil
}

func (r *targetHandlerRegistry) Get(name string) (TargetHandler, error) {
	if h, ok := r.targetHandlers[name]; ok {
		return h, nil
	}

	return nil, fmt.Errorf("output target handler \"%s\" not found", name)
}

func NewTargetHandlerRegistry() TargetHandlerRegistry {
	return &targetHandlerRegistry{
		targetHandlers: make(map[string]TargetHandler),
	}
}
