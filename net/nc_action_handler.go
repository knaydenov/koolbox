package net

import (
	"context"
	backoff "github.com/cenkalti/backoff/v4"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/render"
	_net "net"
	"time"
)

type NcActionHandlerConfig struct {
	Addr        string `doc:"Network address to connect. Templating supported."`
	DialTimeout string `mapstructure:"dial_timeout" default:"1s" doc:"Dial timeout. Dial forever if set to 0."`
	Timeout     string `default:"0s" doc:"Backoff retry timeout. Retry forever if set to 0."`
	MaxInterval string `mapstructure:"max_interval" default:"10s" doc:"Backoff retry max interval."`
	MaxRetries  uint64 `mapstructure:"max_retries" default:"100" doc:"Backoff retry max retries."`
}

func NcActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c NcActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	dialTimeout, err := time.ParseDuration(c.DialTimeout)
	if err != nil {
		return nil, err
	}

	timeout, err := time.ParseDuration(c.Timeout)
	if err != nil {
		return nil, err
	}

	maxInterval, err := time.ParseDuration(c.MaxInterval)
	if err != nil {
		return nil, err
	}

	c.Addr, err = render.Render(c.Addr)
	if err != nil {
		return nil, err
	}

	handlerData["Add"] = c.Addr
	handlerData["DialTimeout"] = dialTimeout
	handlerData["Timeout"] = timeout
	handlerData["MaxInterval"] = maxInterval
	handlerData["MaxRetries"] = c.MaxRetries

	b := backoff.NewExponentialBackOff()
	b.MaxElapsedTime = timeout
	b.MaxInterval = maxInterval

	logger.Info("msg", "Starting a retry loop", "Addr", c.Addr, "DialTimeout", c.DialTimeout, "Timeout", c.Timeout, "MaxInterval", c.MaxInterval, "MaxRetries", c.MaxRetries)

	err = backoff.Retry(makeTryDial(c.Addr, dialTimeout), backoff.WithMaxRetries(b, c.MaxRetries))
	if err != nil {
		logger.Error("msg", "Dial failed", "Addr", c.Addr, "Timeout", timeout)
		return nil, err
	}

	logger.Info("msg", "Dial succeeded", "Addr", c.Addr)

	return handlerData, nil
}

func makeTryDial(address string, timeout time.Duration) func() error {
	return func() error {
		conn, err := _net.DialTimeout("tcp", address, timeout)
		if err != nil {
			logger.Error("msg", "Dial failed", "Addr", address, "DialTimeout", timeout)
			return err
		}
		defer conn.Close()

		return nil
	}
}
