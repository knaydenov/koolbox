package help

import (
	"gitlab.com/knaydenov/koolbox/output"
	"gitlab.com/knaydenov/koolbox/syslog"
)

var SyslogOutputExample = []output.Target{
	{
		Name:              "syslog",
		Type:              "syslog",
		EnabledExpression: "{{ env \"SYSLOG_ENABLED\" }}",
		Filters: []string{
			"^SOME_PREFIX",
			"^OTHER_PREFIX",
		},
		Config: syslog.TargetHandlerConfig{
			Tag:      "some_tag",
			Address:  "localhost:514",
			Facility: "local0",
			Network:  "udp",
			Severity: "warn",
		},
	},
}
