package doc

import (
	"fmt"
	"gitlab.com/knaydenov/koolbox/localization"
	"gopkg.in/yaml.v3"
	"reflect"
	"strconv"
	"strings"
)

func buildNode(c any) *yaml.Node {
	if c == nil {
		return &yaml.Node{}
	}

	t := reflect.TypeOf(c)

	n := yaml.Node{
		Content: []*yaml.Node{},
	}

	switch t.Kind() {
	case reflect.Slice:
		n.Kind = yaml.SequenceNode
		n.Content = []*yaml.Node{}

		for i := range InterfaceSlice(c) {
			n.Content = append(n.Content, buildNode(InterfaceSlice(c)[i]))
		}
	case reflect.Map:
		n.Kind = yaml.MappingNode
		for k, v := range InterfaceMap(c) {
			n.Content = append(n.Content, &yaml.Node{
				Kind:  yaml.ScalarNode,
				Value: k,
			})

			n.Content = append(n.Content, buildNode(v))
		}
	case reflect.Struct:
		n.Kind = yaml.MappingNode
		v := reflect.ValueOf(c)
		tt := reflect.TypeOf(c)
		for i := 0; i < v.NumField(); i++ {
			fieldName := strings.ToLower(v.Type().Field(i).Name)
			mapstructureFieldName, ok := v.Type().Field(i).Tag.Lookup("mapstructure")
			if ok {
				fieldName = mapstructureFieldName
			}
			nn := yaml.Node{
				Kind:  yaml.ScalarNode,
				Value: fieldName,
			}

			comment := localization.LocalizeMessage(tt.Field(i).Tag.Get("doc"))
			defaultValue, ok := tt.Field(i).Tag.Lookup("default")
			if ok {
				comment = fmt.Sprintf("%s [%s: %s]", localization.LocalizeMessage(comment), localization.LocalizeMessage("Default"), defaultValue)
			}

			switch v.Field(i).Kind() {
			default:
				nn.LineComment = comment
			}

			n.Content = append(n.Content, &nn)
			n.Content = append(n.Content, buildNode(v.Field(i).Interface()))
		}
	case reflect.String:
		n.Kind = yaml.ScalarNode
		n.Value = c.(string)
	case reflect.Bool:
		n.Kind = yaml.ScalarNode
		n.Value = strconv.FormatBool(c.(bool))
	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		n.Kind = yaml.ScalarNode
		n.Value = strconv.FormatInt(c.(int64), 10)
	case reflect.Uint:
		fallthrough
	case reflect.Uint8:
		fallthrough
	case reflect.Uint16:
		fallthrough
	case reflect.Uint32:
		fallthrough
	case reflect.Uint64:
		n.Kind = yaml.ScalarNode
		n.Value = strconv.FormatUint(c.(uint64), 10)
	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		n.Kind = yaml.ScalarNode
		n.Value = strconv.FormatFloat(c.(float64), 'E', -1, 32)
	case reflect.Uintptr:
		fallthrough
	case reflect.Complex64:
		fallthrough
	case reflect.Complex128:
		fallthrough
	case reflect.Array:
		fallthrough
	case reflect.Chan:
		fallthrough
	case reflect.Func:
		fallthrough
	case reflect.Interface:
		fallthrough
	case reflect.Pointer:
		fallthrough
	case reflect.UnsafePointer:
		fallthrough
	case reflect.Invalid:
		fallthrough
	default:
		print(t.Kind().String())
		panic("unhandled default case")
	}

	return &n
}

func buildDocument(c any) *yaml.Node {
	return &yaml.Node{
		Kind:    yaml.DocumentNode,
		Content: []*yaml.Node{buildNode(c)},
	}
}

func BuildYaml(c any) (string, error) {
	bytes, err := yaml.Marshal(buildNode(c))
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func InterfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}

	// Keep the distinction between nil and empty slice input
	if s.IsNil() {
		return nil
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}

func InterfaceMap(m any) map[string]any {
	v := reflect.ValueOf(m)
	if v.Kind() != reflect.Map {
		panic("InterfaceMap() given a non-map type")
	}

	// Keep the distinction between nil and empty slice input
	if v.IsNil() {
		return nil
	}

	ret := make(map[string]interface{}, v.Len())

	for _, k := range v.MapKeys() {
		ret[k.String()] = v.MapIndex(k).Interface()
	}

	return ret
}
