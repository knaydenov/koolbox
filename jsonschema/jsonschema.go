package jsonschema

import (
	"github.com/santhosh-tekuri/jsonschema/v5"
	"strings"
)

func ToJsonSchema(schema string) *Schema {
	var err error
	c := jsonschema.NewCompiler()

	if err = c.AddResource("https://schema.json", strings.NewReader(schema)); err != nil {
		return nil
	}

	c.ExtractAnnotations = true

	s, err := c.Compile("https://schema.json")
	if err != nil {
		return nil
	}

	sx := Schema(*s)
	return &sx
}
