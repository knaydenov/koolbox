package data

import (
	"context"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/render"
)

type ManualSourceConfig struct {
	Data map[string]string `doc:"Map of variables. Templating supported."`
}

func ManualSource(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c ManualSourceConfig
	var d = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	for k, v := range c.Data {
		d[k], err = render.Render(v)
		if err != nil {
			return nil, err
		}
	}

	return d, nil
}
