package localization

import (
	"embed"
	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
	"os"
	"strings"
)

//go:embed locale.*.toml
var LocaleFS embed.FS

var localizer *i18n.Localizer

type Options struct {
	Language language.Tag
}

func init() {
	tag := language.MustParse("ru_RU")

	err := Configure(Options{
		Language: tag,
	})
	if err != nil {
		panic(err)
	}
}

func Configure(options Options) error {
	var err error

	bundle := i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)

	localeFiles := []string{"locale.en.toml", "locale.ru.toml"}
	for _, localeFile := range localeFiles {
		err = registerMessageFile(bundle, localeFile)
		if err != nil {
			return err
		}
	}

	langParts := strings.Split(os.Getenv("LANG"), ".")
	localizer = i18n.NewLocalizer(bundle, langParts[0], "en")

	return nil
}

func registerMessageFile(bundle *i18n.Bundle, path string) error {
	var err error

	_, err = bundle.LoadMessageFileFS(LocaleFS, path)
	if err != nil {
		return err
	}

	return nil
}

func LocalizeMessage(message string, templateData ...map[string]any) string {
	var err error
	if len(templateData) == 0 {
		templateData = []map[string]any{{}}
	}
	localizedMessage, err := localizer.Localize(&i18n.LocalizeConfig{
		MessageID:    message,
		TemplateData: templateData[0],
	})
	if err != nil {
		return message
	}
	return localizedMessage
}

func GetLocalizer() *i18n.Localizer {
	return localizer
}
