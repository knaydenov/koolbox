package syslog

import (
	"errors"
	"fmt"
	syslogcommon "github.com/leodido/go-syslog/v4/common"
	"github.com/leodido/go-syslog/v4/rfc5424"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// A Writer is a connection to a syslog server.
type Writer struct {
	version       uint16
	messageFormat uint8
	priority      uint8
	tag           string
	hostname      string
	network       string
	raddr         string
	procid        string
	msgid         string

	mu   sync.Mutex
	conn net.Conn
}

var MessageFormatKeywords = map[uint8]string{
	0: "rfc5424",
	1: "rfc3164",
}

var MessageFormatValues = makeRevertedMap(MessageFormatKeywords)
var FacilityValues = makeRevertedMap(syslogcommon.FacilityKeywords)
var SeverityValues = makeRevertedMap(syslogcommon.SeverityLevelsShort)

func makeRevertedMap(strait map[uint8]string) map[string]uint8 {
	reverse := map[string]uint8{}

	for k, v := range strait {
		reverse[v] = k
	}
	return reverse
}

func safeGetValueByKeyword(m map[string]uint8, keyword string) (uint8, error) {
	val, ok := m[keyword]
	if ok {
		return val, nil
	}
	return 0, fmt.Errorf("keyword %s not found", keyword)
}

func geMessageFormatByKeyword(keyword string) (uint8, error) {
	return safeGetValueByKeyword(MessageFormatValues, keyword)
}

func getFacilityByKeyword(keyword string) (uint8, error) {
	return safeGetValueByKeyword(FacilityValues, keyword)
}

func getSeverityByKeyword(keyword string) (uint8, error) {
	return safeGetValueByKeyword(SeverityValues, keyword)
}

func getPriority(facility, severity string) (uint8, error) {
	var err error
	var priority uint8

	if facility == "" || severity == "" {
		err = fmt.Errorf("facility and severity must be non-empty")
		return 0, err
	}

	facility = strings.ToLower(facility)
	severity = strings.ToLower(severity)

	facilityValue, err := getFacilityByKeyword(facility)
	if err != nil {
		return 0, fmt.Errorf("unknown facility %s", facility)
	}

	severityValue, err := getSeverityByKeyword(severity)
	if err != nil {
		return 0, fmt.Errorf("unknown severity %s", severity)
	}

	priority = facilityValue | severityValue

	return priority, nil
}

func NewWriter(network, raddr string, version uint16, priority uint8, tag string, procid string, msgid string, format string) (*Writer, error) {
	var err error

	if !syslogcommon.ValidPriority(priority) {
		return nil, fmt.Errorf("invalid priority value: %d", priority)
	}

	if tag == "" {
		tag = "unknown"
	}

	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	messageFormat, err := geMessageFormatByKeyword(format)
	if err != nil {
		return nil, err
	}

	if procid == "" {
		procid = strconv.Itoa(os.Getpid())
	}

	w := &Writer{
		messageFormat: messageFormat,
		version:       version,
		priority:      priority,
		tag:           tag,
		procid:        procid,
		msgid:         msgid,
		hostname:      hostname,
		network:       network,
		raddr:         raddr,
	}

	w.mu.Lock()
	defer w.mu.Unlock()

	err = w.connect()
	if err != nil {
		return nil, err
	}
	return w, err
}

func (w *Writer) connect() (err error) {
	if w.conn != nil {
		w.conn.Close()
		w.conn = nil
	}

	w.conn, err = net.Dial(w.network, w.raddr)
	if err != nil {
		return err
	}

	return
}

func (w *Writer) Write(b []byte) (int, error) {
	return w.writeAndRetry(w.priority, string(b))
}

func (w *Writer) Close() error {
	var err error

	w.mu.Lock()
	defer w.mu.Unlock()

	if w.conn != nil {
		err = w.conn.Close()
		if err != nil {
			return err
		}
		w.conn = nil
	}

	return nil
}

func (w *Writer) writeAndRetry(p uint8, s string) (int, error) {
	var err error

	w.mu.Lock()
	defer w.mu.Unlock()

	if w.conn != nil {
		err = w.writeString(w.version, p, w.hostname, w.tag, w.msgid, s, w.procid)
		if err != nil {
			return 0, err
		}
	}

	err = w.connect()
	if err != nil {
		return 0, err
	}

	return len(s), w.writeString(w.version, p, w.hostname, w.tag, w.msgid, s, w.procid)
}

func makeRFC3164MessageString(m *rfc5424.SyslogMessage) (string, error) {
	return fmt.Sprintf(
		"<%d>%s %s %s[%s]: %s",
		*m.Priority,
		*m.Timestamp,
		*m.Hostname,
		*m.Appname,
		*m.ProcID,
		*m.Message,
	), nil
}

func makeRFC5424MessageString(m *rfc5424.SyslogMessage) (string, error) {
	return m.String()
}

func (w *Writer) makeMessageString(m *rfc5424.SyslogMessage) (string, error) {
	if !m.Valid() {
		return "", errors.New("invalid syslog message")
	}

	if w.messageFormat == 0 {
		return makeRFC5424MessageString(m)
	}

	if w.messageFormat == 1 {
		return makeRFC3164MessageString(m)
	}

	return "", fmt.Errorf("invalid syslog message format: %d", w.messageFormat)
}

func (w *Writer) writeString(version uint16, p uint8, hostname string, tag string, msgid string, msg string, procid string) error {
	var err error
	m := rfc5424.SyslogMessage{}

	m.SetVersion(version)
	m.SetPriority(p)
	m.SetHostname(hostname)
	m.SetAppname(tag)
	m.SetMsgID(msgid)
	m.SetMessage(msg)
	m.SetTimestamp(time.Now().Format(time.RFC3339))
	m.SetProcID(procid)

	s, err := w.makeMessageString(&m)
	if err != nil {
		return err
	}

	_, err = fmt.Fprint(w.conn, s)
	if err != nil {
		return err
	}

	return nil
}
