package output

import (
	"fmt"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/storage"
	"io"
	"time"
)

type Options struct {
	Path string
}

var thr TargetHandlerRegistry

var writer io.Writer

func GetWriter() io.Writer {
	return writer
}

func Configure(options Options) {
	var err error
	var targets []Target
	var writers []io.Writer

	err = mapstructure.Decode(viper.Get("output"), &targets)
	cobra.CheckErr(err)

	targets = append(targets, Target{Name: "stdout", Type: "stdout", Enabled: true, FilterBuffer: FilterBufferConfig{}})

	for i, _ := range targets {
		defaults.SetDefaults(&targets[i])
	}

	for i, t := range targets {
		t.Name = helper.DefaultString(t.Name, fmt.Sprintf("target_%s_%d", t.Type, i))

		if t.EnabledExpression != "" {
			t.Enabled, err = helper.EvaluateExpressionAsBool(t.EnabledExpression)
			cobra.CheckErr(err)
		}

		if t.Enabled {
			w, err := initTarget(t)
			cobra.CheckErr(err)

			logger.Debug("msg", "Adding output destination", "name", t.Name)

			writers = append(writers, w)
		}
	}

	storage.Get().Set("output", targets)

	writer = io.MultiWriter(writers...)
}

func initTarget(target Target) (io.Writer, error) {
	var err error
	var handler TargetHandler

	handler, err = thr.Get(target.Type)
	cobra.CheckErr(err)

	w, err := handler(target.Config)
	cobra.CheckErr(err)

	minInterval, err := time.ParseDuration(target.FilterBuffer.MinInterval)
	cobra.CheckErr(err)

	maxInterval, err := time.ParseDuration(target.FilterBuffer.MaxInterval)
	cobra.CheckErr(err)

	if minInterval > maxInterval {
		return nil, fmt.Errorf("filter interval must be less than filter interval")
	}

	if len(target.Filters) > 0 {
		return NewFilteringWriter(w, target.Filters, minInterval, maxInterval), nil
	}

	return w, nil
}
