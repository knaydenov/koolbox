package exec

import (
	"bytes"
	"context"
	"fmt"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/output"
	"gitlab.com/knaydenov/koolbox/render"
	"io"
	"os/exec"
)

type ActionHandlerConfig struct {
	Cmd           string   `doc:"Command for execution (bash, echo, etc.). Templating supported."`
	Args          []string `doc:"Command Arguments. Templating supported."`
	PrintOutput   bool     `mapstructure:"print_output" default:"false" doc:"Is command's output should be printed to stdout?"`
	FetchOutput   bool     `mapstructure:"fetch_output" default:"false" doc:"Is command's output should be saved to vars?"`
	ForwardOutput struct {
		Enabled bool   `default:"false"`
		Network string `default:"tcp"`
		Address string `default:"localhost:5170"`
		Timeout string `default:"0s"`
	} `mapstructure:"forward_output" doc:"Deprecated: moved to \"output\" section"`
}

func ActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c ActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Cmd, err = render.Render(c.Cmd)
	if err != nil {
		return nil, err
	}

	for n, arg := range c.Args {
		c.Args[n], err = render.Render(arg)
		if err != nil {
			return nil, err
		}
	}

	cmd := exec.Command(c.Cmd, c.Args...)
	stdout, stderr, err := executeCommand(cmd, c.PrintOutput, c.FetchOutput, c)
	if err != nil {
		return nil, err
	}

	handlerData["ExitCode"] = cmd.ProcessState.ExitCode()
	handlerData["Stdout"] = stdout
	handlerData["Stderr"] = stderr

	return handlerData, nil
}

func executeCommand(cmd *exec.Cmd, printOutput bool, fetchOutput bool, x ActionHandlerConfig) (string, string, error) {
	var err error

	fetchStdoutWriter := bytes.NewBufferString("")
	fetchStderrWriter := bytes.NewBufferString("")

	if printOutput {
		cmd.Stdout = output.GetWriter()
		cmd.Stderr = output.GetWriter()
	}

	if fetchOutput {
		if cmd.Stdout != nil {
			cmd.Stdout = io.MultiWriter(cmd.Stdout, fetchStdoutWriter)
		} else {
			cmd.Stdout = fetchStdoutWriter
		}

		if cmd.Stderr != nil {
			cmd.Stderr = io.MultiWriter(cmd.Stderr, fetchStderrWriter)
		} else {
			cmd.Stderr = fetchStderrWriter
		}
	}

	err = cmd.Start()
	if err != nil {
		return "", "", err
	}

	logger.Info("msg", "Command execution started", "name", cmd.Path, "pid", cmd.Process.Pid)
	for i, arg := range cmd.Args {
		logger.Debug(fmt.Sprintf("arg%d", i), arg)
	}

	err = cmd.Wait()
	if err != nil {
		return "", "", err
	}

	logger.Info("msg", "Command execution ended")

	return fetchStdoutWriter.String(), fetchStderrWriter.String(), nil
}
