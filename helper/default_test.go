package helper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDefaultString(t *testing.T) {
	assert.Equal(t, DefaultString("", "hello"), "hello")
	assert.Equal(t, DefaultString("hello", "other"), "hello")
	assert.Equal(t, DefaultString("", ""), "")
}
