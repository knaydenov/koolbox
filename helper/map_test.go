package helper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)



func TestFilterMap(t *testing.T) {
	table := []struct {
		data map[string]interface{}
		filters []string
		ret map[string]interface{}
		err error
	}{
		{
			map[string]interface{} {"key1": "value1"},
			[]string{".*"},
			map[string]interface{} {"key1": "value1"},
			nil,
		},
		{
			map[string]interface{} {"key1": "value1", "key2": "value2", "key3": "value3"},
			[]string{".*"},
			map[string]interface{} {"key1": "value1", "key2": "value2", "key3": "value3"},
			nil,
		},
		{
			map[string]interface{} {"foo": "value1", "bar": "value2", "baz": "value3"},
			[]string{"ba."},
			map[string]interface{} {"bar": "value2", "baz": "value3"},
			nil,
		},
		{
			map[string]interface{} {"foo": "value1", "bar": "value2", "baz": "value3"},
			[]string{"ba.", "f.*"},
			map[string]interface{} {"foo": "value1", "bar": "value2", "baz": "value3"},
			nil,
		},
		{
			map[string]interface{} {"foo": "value1", "bar": "value2", "baz": "value3"},
			[]string{"ba.", "bar"},
			map[string]interface{} {"bar": "value2", "baz": "value3"},
			nil,
		},
		// TODO: Add more tests
	}

	for _, row := range table {
		ret, err := FilterMap(&row.data, row.filters)
		assert.Equal(t, ret, row.ret)
		assert.Equal(t, err, row.err)
	}
}

