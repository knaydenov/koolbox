package help

import (
	"gitlab.com/knaydenov/koolbox/output"
	"gitlab.com/knaydenov/koolbox/task"
)

type Config struct {
	Output []output.Target `doc:"Output configuration."`
	Tasks  []task.Task     `doc:"Tasks configuration."`
}

func GetTasks() []task.Task {
	var example []task.Task

	example = append(example, NetTasksExample...)
	example = append(example, ExecTaskExample...)
	example = append(example, DataTaskExample...)
	example = append(example, DebugTaskExample...)

	return example
}

func GetOutput() []output.Target {
	var example []output.Target

	example = append(example, SyslogOutputExample...)

	return example
}

func GetConfig() Config {
	return Config{
		Output: GetOutput(),
		Tasks:  GetTasks(),
	}
}
