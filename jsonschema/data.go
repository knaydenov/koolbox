package jsonschema

import (
	"github.com/santhosh-tekuri/jsonschema/v5"
	"net/url"
	"strings"
)

type Schema jsonschema.Schema

type row struct {
	self   *jsonschema.Schema
	parent *jsonschema.Schema
}

type Field struct {
	Field       string
	Description string
	Required    bool
	Default     any
	Examples    any
	Enum        any
}

func (s *Schema) AsFields() ([]Field, error) {
	var rows []Field

	for _, row := range flatten((*jsonschema.Schema)(s), nil) {
		fieldPath, err := getFieldPath(row.self)
		if err != nil {
			return nil, err
		}

		fieldRequired, err := isFieldRequired(row.self, row.parent)
		if err != nil {
			return nil, err
		}

		rows = append(rows, Field{
			Field:       fieldPath,
			Description: row.self.Description,
			Required:    fieldRequired,
			Default:     row.self.Default,
			Examples:    row.self.Examples,
			Enum:        row.self.Enum,
		})
	}

	return rows, nil
}

func flatten(s *jsonschema.Schema, p *jsonschema.Schema) []row {
	var rows []row

	if p != nil {
		rows = append(rows, row{
			self:   s,
			parent: p,
		})
	}

	for _, child := range s.Properties {
		rows = append(rows, flatten(child, s)...)
	}

	return rows
}

func isFieldRequired(s *jsonschema.Schema, p *jsonschema.Schema) (bool, error) {
	var err error

	fieldName, err := getFieldName(s)
	if err != nil {
		return false, err
	}

	for _, requiredFieldName := range p.Required {
		if fieldName == requiredFieldName {
			return true, nil
		}
	}
	return false, nil
}

func getFieldPath(s *jsonschema.Schema) (string, error) {
	var err error

	u, err := url.Parse(s.Location)
	if err != nil {
		return "", err
	}

	parts := strings.Split(u.Fragment, "/properties/")

	return strings.Join(parts, "."), nil
}

func getFieldName(s *jsonschema.Schema) (string, error) {
	var err error

	path, err := getFieldPath(s)
	if err != nil {
		return "", err
	}

	parts := strings.Split(path, ".")

	return parts[len(parts)-1], nil
}
