package net

import (
	"context"
	"fmt"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/render"
	"io"
	"net/http"
	"os"
	"path"
)

type DownloadActionHandlerConfig struct {
	Src       string `doc:"Source URL. Templating supported."`
	Dst       string `doc:"Destination path. Templating supported."`
	CreateDir bool   `mapstructure:"create_dir" default:"true" doc:"Should the target directory be created?"`
}

func DownloadActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c DownloadActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	c.Src, err = render.Render(c.Src)
	if err != nil {
		return nil, err
	}

	c.Dst, err = render.Render(c.Dst)
	if err != nil {
		return nil, err
	}

	logger.Info("msg", "Downloading file", "Src", c.Src, "Dst", c.Dst)

	resp, err := http.Get(c.Src)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	logger.Info("msg", "Downloaded file", "Src", c.Src, "Dst", c.Dst, "FileSize", resp.ContentLength)

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status: %s", resp.Status)
		return nil, err
	}

	logger.Info("msg", "Saving file", "Src", c.Src, "Dst", c.Dst)

	dstDir := path.Dir(c.Dst)

	err = helper.EnsureDirExists(dstDir, c.CreateDir)
	if err != nil {
		return nil, err
	}

	dstFile, err := os.Create(c.Dst)
	if err != nil {
		return nil, err
	}
	defer dstFile.Close()

	bytesWritten, err := io.Copy(dstFile, resp.Body)
	if err != nil {
		return nil, err
	}

	logger.Info("msg", "Saved file", "Src", c.Src, "Dst", c.Dst, "FileSize", bytesWritten)

	handlerData["Src"] = c.Src
	handlerData["Dst"] = c.Dst
	handlerData["FileSize"] = bytesWritten

	return handlerData, nil
}
