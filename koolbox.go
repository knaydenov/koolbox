package main

import _ "github.com/joho/godotenv/autoload"
import "gitlab.com/knaydenov/koolbox/cmd"

func main() {
	cmd.Execute()
}
