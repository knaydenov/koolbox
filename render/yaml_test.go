package render

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var yaml1 = `
emptyObject: {}
nullObject:
nullObject2: null
emptyList: []
emptyString: ""
boolFalse: false
boolTrue: true
number: 1
float: 1.0
nestedObject:
  nestedObject2: nested
`

func TestFromYaml(t *testing.T) {
	data, ok := fromYaml(yaml1).(map[string]interface{})

	assert.True(t, ok)

	assert.Equal(t, map[string]interface{}{}, data["emptyObject"])
	assert.Equal(t, nil, data["nullObject"])
	assert.Equal(t, []interface{}{}, data["emptyList"])
}

func TestToYaml(t *testing.T) {
	data := toYaml(map[string]interface{}{"key": "value"})

	assert.Equal(t, "key: value\n", data)
}
