package doc

import (
	"fmt"
	"strings"
)

type Section struct {
	Header  string
	Content string
	Level   int
}

func RenderCodeBlock(c string, t string) string {
	return "```" + t + "\n" + c + "```"
}

func RenderMarkdown(s []Section, l int) string {
	var ss []string

	for i := range s {
		ss = append(ss, fmt.Sprintf("%s %s", strings.Repeat("#", l+s[i].Level), s[i].Header))
		ss = append(ss, s[i].Content)
	}

	return strings.Join(ss, "\n")
}
