package render

import (
	"bytes"
	"encoding/json"
	"gitlab.com/knaydenov/koolbox/files"
	"gitlab.com/knaydenov/koolbox/jsonschema"
	"gitlab.com/knaydenov/koolbox/localization"
	"os"
	"text/template"
)

var genericMap = map[string]interface{}{
	"kebab2camel": kebab2Camel,
	"kebab2snake": kebab2Snake,
	"camel2kebab": camel2Kebab,
	"camel2snake": camel2Snake,
	"snake2kebab": snake2Kebab,
	"snake2camel": snake2Camel,

	"prefix": addPrefix,
	"suffix": addSuffix,

	"format": format,

	"filter_map":      filterMap,
	"add_prefix_map":  addPrefixMap,
	"add_suffix_map":  addSuffixMap,
	"trim_prefix_map": trimPrefixMap,
	"trim_suffix_map": trimSuffixMap,

	"env":    env,
	"toJson": toJson,

	"fromYaml": fromYaml,
	"toYaml":   toYaml,

	"toJsonSchema": jsonschema.ToJsonSchema,

	"files": files.Files,

	"localize": localization.LocalizeMessage,
}

func GenericFuncMap() map[string]interface{} {
	gfm := make(map[string]interface{}, len(genericMap))
	for k, v := range genericMap {
		gfm[k] = v
	}
	return gfm
}

func TxtFuncMap() template.FuncMap {
	return GenericFuncMap()
}

func env(name string) string {
	return os.Getenv(name)
}

func toJson(data interface{}, args ...string) string {
	var err error
	var b []byte
	var prefix string
	var indent string

	if len(args) > 0 {
		prefix = args[0]
	}

	if len(args) > 1 {
		indent = args[1]
	} else {
		indent = "  "
	}

	b, err = json.MarshalIndent(data, prefix, indent)
	if err != nil {
		return ""
	}

	return string(b)
}

func makeIncludeFunc(t *template.Template) func(name string, data interface{}) (string, error) {
	return func(name string, data interface{}) (string, error) {
		buf := bytes.NewBuffer(nil)
		if err := t.ExecuteTemplate(buf, name, data); err != nil {
			return "", err
		}
		return buf.String(), nil
	}
}
