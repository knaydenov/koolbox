# koolbox

This is a command line tool that helps to configure Kubernetes pods.

## Quick start

Create a `.koolbox` file

```yaml
- action: exec
  config:
    cmd: echo
    args: ['Hello']
```

Run

```shell
koolbox run
```

## Command reference

[https://knaydenov.gitlab.io/koolbox/koolbox.html](https://knaydenov.gitlab.io/koolbox/koolbox.html)