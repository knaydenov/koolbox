package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/knaydenov/koolbox/cmd/run"
	"gitlab.com/knaydenov/koolbox/cmd/version"
	"gitlab.com/knaydenov/koolbox/config"
	"gitlab.com/knaydenov/koolbox/localization"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/output"
	"gitlab.com/knaydenov/koolbox/storage"
	"io"
	"os"
)

type Options struct {
}

func NewCmd(options Options) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "koolbox",
		Short: localization.LocalizeMessage("Koolbox — an advanced entrypoint"),
		Long:  localization.LocalizeMessage("Koolbox — an advanced entrypoint. It contains various tools helping set up environment and run tasks."),

		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			initLogger(os.Stdout)
			initConfig()
			initOutput()

			storage.Get().Set("koolbox", map[string]any{
				"settings": viper.AllSettings(),
			})

			logger.Debug("config", viper.ConfigFileUsed())
		},
	}

	// Flags
	cmd.PersistentFlags().String("config", "", localization.LocalizeMessage("Config file"))
	cmd.PersistentFlags().CountP("verbose", "v", localization.LocalizeMessage("Verbosity level"))

	viper.BindPFlag("logging.verbosity", cmd.PersistentFlags().Lookup("verbose"))
	viper.BindPFlag("config", cmd.PersistentFlags().Lookup("config"))
	viper.BindEnv("logging.verbosity", "KOOLBOX_VERBOSITY")

	// Commands
	cmd.AddCommand(run.NewCmd(run.Options{}))
	cmd.AddCommand(version.NewCmd(version.Options{}))

	cmd.SetUsageTemplate(getUsageTemplate())

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {

}

func Execute() {
	rootCmd := NewCmd(Options{})

	err := rootCmd.Execute()
	cobra.CheckErr(err)
}

func initLogger(writer io.Writer) {
	logger.NewLogger(logger.Options{
		Verbosity: viper.GetInt("logging.verbosity"),
		Writer:    writer,
	})
}

func initConfig() {
	config.Configure(config.Options{
		Path: viper.GetString("config"),
	})
}

func initOutput() {
	output.Configure(output.Options{})
}
