package render

import (
	"fmt"
	"gitlab.com/knaydenov/koolbox/helper"
)

func format(tokens []interface{}, s string) string {
	return fmt.Sprintf(s, tokens...)
}

func addPrefix(prefix string, s string) string {
	return helper.AddPrefix(prefix, s)
}

func addSuffix(suffix string, s string) string {
	return helper.AddSuffix(suffix, s)
}
