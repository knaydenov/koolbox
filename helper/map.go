package helper

import (
	"regexp"
	"strings"
)

func FilterMap(data *map[string]interface{}, filters []string) (map[string]interface{}, error) {
	var filteredData = make(map[string]interface{})

	if len(filters) == 0 {
		filters = append(filters, ".*")
	}

	for _, f := range filters {
		r, err := regexp.Compile(f)
		if err != nil {
			return nil, err
		}

		for k, v := range *data {
			if r.MatchString(k) {
				filteredData[k] = v
			}
		}
	}

	return filteredData, nil
}

func AddPrefixMap(data *map[string]interface{}, prefix string) (map[string]interface{}, error) {
	var prefixedData = make(map[string]interface{})

	for k, v := range *data {
		prefixedData[AddPrefix(prefix, k)] = v
	}

	return prefixedData, nil
}

func AddSuffixMap(data *map[string]interface{}, suffix string) (map[string]interface{}, error) {
	var prefixedData = make(map[string]interface{})

	for k, v := range *data {
		prefixedData[AddSuffix(suffix, k)] = v
	}

	return prefixedData, nil
}

func TrimPrefixMap(data *map[string]interface{}, prefix string) (map[string]interface{}, error) {
	var trimPrefixData = make(map[string]interface{})

	for k, v := range *data {
		trimPrefixData[strings.TrimPrefix(k, prefix)] = v
	}

	return trimPrefixData, nil
}

func TrimSuffixMap(data *map[string]interface{}, suffix string) (map[string]interface{}, error) {
	var trimSuffixData = make(map[string]interface{})

	for k, v := range *data {
		trimSuffixData[strings.TrimSuffix(k, suffix)] = v
	}

	return trimSuffixData, nil
}
