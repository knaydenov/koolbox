package render

import (
	"context"
	"github.com/Masterminds/sprig/v3"
	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/logger"
	"os"
	"path"
	"text/template"
)

type ActionHandlerConfig struct {
	Src       string `doc:"Source template path. Templating supported."`
	Dst       string `doc:"Destination path. Templating supported."`
	CreateDir bool   `mapstructure:"create_dir" default:"true" doc:"Should the destination directory be created?"`
}

func ActionHandler(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c ActionHandlerConfig
	var handlerData = make(map[string]interface{})

	defaults.SetDefaults(&c)

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	var src string
	var dst string

	src, err = Render(c.Src)
	if err != nil {
		return nil, err
	}

	dst, err = Render(c.Dst)
	if err != nil {
		return nil, err
	}

	logger.Info("msg", "Rendering template", "src", src, "dst", dst)

	var t = template.New(path.Base(src))
	var funcs = template.FuncMap{
		"include": makeIncludeFunc(t),
	}

	t, err = t.Funcs(funcs).Funcs(sprig.TxtFuncMap()).Funcs(TxtFuncMap()).ParseFiles(src)
	if err != nil {
		return nil, err
	}

	outputDir := path.Dir(dst)

	err = helper.EnsureDirExists(outputDir, c.CreateDir)
	if err != nil {
		return nil, err
	}

	var f *os.File

	f, err = os.Create(dst)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	err = t.Execute(f, data)
	if err != nil {
		return nil, err
	}

	handlerData = map[string]interface{}{
		"Src": src,
		"Dst": dst,
	}

	return handlerData, nil
}
