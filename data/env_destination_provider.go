package data

import (
	"context"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/render"
	"os"
)

type EnvDestinationConfig struct {
	Filters    []string `doc:"Regex filters for variable name."`
	AddPrefix  string   `mapstructure:"add_prefix" doc:"Add prefix to environment variable name. Templating supported."`
	AddSuffix  string   `mapstructure:"add_suffix" doc:"Add suffix to environment variable name. Templating supported."`
	TrimPrefix string   `mapstructure:"trim_prefix" doc:"Remove prefix to environment variable name. Templating supported."`
	TrimSuffix string   `mapstructure:"trim_suffix" doc:"Remove suffix to environment variable name. Templating supported."`
}

func EnvDestination(ctx context.Context, config interface{}, data map[string]interface{}) error {
	var err error
	var c EnvDestinationConfig
	var resultData map[string]interface{}

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return err
	}

	c.AddPrefix, err = render.Render(c.AddPrefix)
	if err != nil {
		return err
	}

	c.AddSuffix, err = render.Render(c.AddSuffix)
	if err != nil {
		return err
	}

	c.TrimPrefix, err = render.Render(c.TrimPrefix)
	if err != nil {
		return err
	}

	c.TrimSuffix, err = render.Render(c.TrimSuffix)
	if err != nil {
		return err
	}

	resultData, err = helper.FilterMap(&data, c.Filters)
	if err != nil {
		return err
	}

	if c.TrimPrefix != "" {
		resultData, err = helper.TrimPrefixMap(&resultData, c.TrimPrefix)
		if err != nil {
			return err
		}
	}

	if c.TrimSuffix != "" {
		resultData, err = helper.TrimSuffixMap(&resultData, c.TrimSuffix)
		if err != nil {
			return err
		}
	}

	if c.AddPrefix != "" {
		resultData, err = helper.AddPrefixMap(&resultData, c.AddPrefix)
		if err != nil {
			return err
		}
	}

	if c.AddSuffix != "" {
		resultData, err = helper.AddSuffixMap(&resultData, c.AddSuffix)
		if err != nil {
			return err
		}

	}

	for k, v := range resultData {
		err = os.Setenv(k, fmt.Sprintf("%s", v))
		if err != nil {
			return err
		}
	}

	return nil
}
