package cmd

import (
	"embed"
	"github.com/Masterminds/sprig/v3"
	"github.com/spf13/cobra"
	"gitlab.com/knaydenov/koolbox/render"
)

//go:embed *.tpl
var LocaleFS embed.FS

func init() {
	cobra.AddTemplateFuncs(sprig.TxtFuncMap())
	cobra.AddTemplateFuncs(render.GenericFuncMap())
	//cobra.AddTemplateFunc("localize", localization.LocalizeMessage)
}

func getUsageTemplate() string {
	template, err := LocaleFS.ReadFile("usage_template.tpl")
	cobra.CheckErr(err)

	return string(template)
}
