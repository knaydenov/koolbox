package data

import (
	"context"
	"fmt"
)

type DestinationProvider func(ctx context.Context, config interface{}, data map[string]interface{}) error

type DestinationProviderRegistry interface {
	Register(name string, destinationProvider DestinationProvider) error
	Get(name string) (DestinationProvider, error)
}

type destinationProviderRegistry struct {
	destinationProviders map[string]DestinationProvider
}

func (r *destinationProviderRegistry) Register(name string, destination DestinationProvider) error {
	r.destinationProviders[name] = destination

	return nil
}

func (r *destinationProviderRegistry) Get(name string) (DestinationProvider, error) {
	if h, ok := r.destinationProviders[name]; ok {
		return h, nil
	}

	return nil, fmt.Errorf("destination \"%s\" not found", name)
}

func NewDestinationProviderRegistry() DestinationProviderRegistry {
	return &destinationProviderRegistry{
		destinationProviders: make(map[string]DestinationProvider),
	}
}

