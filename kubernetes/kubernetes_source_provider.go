package kubernetes

import (
	"context"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/knaydenov/koolbox/helper"
	"gitlab.com/knaydenov/koolbox/render"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type KubernetesSourceConfig struct {
	From      string   `doc:"Kind of source (configmap|secret)."`
	Namespace string   `doc:"Name of namespace."`
	Name      string   `doc:"Name of configmap or secret."`
	Filters   []string `doc:"Regex filters for variables."`
}

func KubernetesSource(ctx context.Context, config interface{}, data map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var c KubernetesSourceConfig
	var returnData = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	clientset, err := getClientset()
	if err != nil {
		return nil, err
	}

	currentNamespace, err := getCurrentNamespace()
	if err != nil {
		return nil, err
	}

	c.Namespace, err = render.Render(c.Namespace)
	if err != nil {
		return nil, err
	}

	c.Namespace = helper.DefaultString(c.Namespace, currentNamespace)

	c.Name, err = render.Render(c.Name)
	if err != nil {
		return nil, err
	}

	switch c.From {
	case "configmap":
		configmap, err := clientset.CoreV1().ConfigMaps(c.Namespace).Get(ctx, c.Name, v1.GetOptions{})
		if err != nil {
			return nil, err
		}

		for k, v := range configmap.Data {
			returnData[k] = v
		}
	case "secret":
		secret, err := clientset.CoreV1().Secrets(c.Namespace).Get(ctx, c.Name, v1.GetOptions{})
		if err != nil {
			return nil, err
		}

		for k, v := range secret.Data {
			returnData[k] = helper.ToString(v)
		}
	}

	filteredData, err := helper.FilterMap(&returnData, c.Filters)
	if err != nil {
		return nil, err
	}

	return filteredData, nil
}
