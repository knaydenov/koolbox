package files

import (
	"github.com/gobwas/glob"
	"os"
	"path/filepath"
)

type File struct {
	Path string
	Data []byte
}

func Files(searchPath string, pattern string) ([]*File, error) {
	var err error
	var fileList []*File

	g, err := glob.Compile(pattern, '/')
	if err != nil {
		g, _ = glob.Compile("**")
	}

	filePaths, err := getFilePaths(searchPath)
	if err != nil {
		return nil, err
	}

	for _, filePath := range filePaths {
		if g.Match(filePath) {

			fileData, err := os.ReadFile(filePath)
			if err != nil {
				return nil, err
			}

			f := File{
				Path: filePath,
				Data: fileData,
			}
			fileList = append(fileList, &f)
		}
	}

	return fileList, nil
}

func getFilePaths(path string) ([]string, error) {
	var err error

	walkFunc, files := makeWalkFunc()
	err = filepath.Walk(path, walkFunc)
	if err != nil {
		return nil, err
	}

	return *files, nil
}

func makeWalkFunc() (filepath.WalkFunc, *[]string) {
	var files []string

	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			files = append(files, path)
		}

		return nil
	}, &files
}
