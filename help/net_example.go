package help

import (
	"gitlab.com/knaydenov/koolbox/net"
	"gitlab.com/knaydenov/koolbox/task"
)

var NetTasksExample = []task.Task{
	{
		Action: "nc",
		Config: net.NcActionHandlerConfig{
			Addr:        "localhost:80",
			DialTimeout: "10s",
			MaxInterval: "5s",
			Timeout:     "30s",
			MaxRetries:  10,
		},
	},
}
