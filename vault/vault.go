package vault

import (
	"crypto/tls"
	"fmt"
	"github.com/hashicorp/vault/api"
	"net/http"
)

type ClientConfig struct {
	Address string
	Auth    struct {
		Type     string
		Username string
		Password string
	}
	HttpClientConfig HttpClientConfig
}

func createClient(c *ClientConfig) (*api.Client, error) {
	var err error

	httpClient, err := createHttpClient(c.HttpClientConfig)
	if err != nil {
		return nil, err
	}

	config := api.Config{
		Address:    c.Address,
		HttpClient: httpClient,
	}

	client, err := api.NewClient(&config)
	if err != nil {
		return nil, err
	}

	switch c.Auth.Type {
	case "userpass":
		err = loginUserpass(client, c.Auth.Username, c.Auth.Password)
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unsupported auth method '%s'", c.Auth.Type)
	}

	return client, nil
}

func createHttpClient(config HttpClientConfig) (*http.Client, error) {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: config.InsecureSkipVerify},
	}

	return &http.Client{Transport: transport}, nil
}

func loginUserpass(c *api.Client, username string, password string) error {
	options := map[string]interface{}{
		"password": password,
	}

	path := fmt.Sprintf("auth/userpass/login/%s", username)

	secret, err := c.Logical().Write(path, options)
	if err != nil {
		return err
	}

	c.SetToken(secret.Auth.ClientToken)

	return nil
}
