package net

import (
	"context"
	"encoding/json"
	"github.com/mitchellh/mapstructure"
)

type JsonHttpDataMarshalerConfig struct{}

func JsonHttpDataMarshaler(ctx context.Context, config interface{}, body []byte) (map[string]interface{}, error) {
	var err error
	var c JsonHttpDataMarshalerConfig
	var d = make(map[string]interface{})

	err = mapstructure.Decode(config, &c)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &d)
	if err != nil {
		return nil, err
	}

	return d, nil
}
