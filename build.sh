#/usr/bin/env bash

git config --global url."https://$KOOLBOX_CI_DEPLOY_TOKEN_USERNAME:$KOOLBOX_CI_DEPLOY_TOKEN_PASSWORD@gitlab.com/".insteadOf https://gitlab.com/

build() {
  make
}

test() {
  make test
}



"${@:-unknown_command}"