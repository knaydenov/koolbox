package helper

import "fmt"

func ToString(value interface{}) string {
	switch v := value.(type) {
	case []byte:
		return string(v)
	case int:
		return string(rune(v))
	case bool:
		if v {
			return "true"
		} else {
			return "false"
		}
	case string:
		return v
	default:
		return ""
	}
}

func AddPrefix(prefix string, s string) string {
	return fmt.Sprintf("%s%s", prefix, s)
}

func AddSuffix(suffix string, s string) string {
	return fmt.Sprintf("%s%s", s, suffix)
}
