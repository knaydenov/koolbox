package helper

import (
	"fmt"
	"github.com/expr-lang/expr"
	"gitlab.com/knaydenov/koolbox/logger"
	"gitlab.com/knaydenov/koolbox/storage"
	"os"
)

// Deprecated: Use EvaluateExpression instead
func SubstituteExpression(expression string, data *map[string]interface{}) (string, error) {
	value, err := expr.Eval(expression, data)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%v", value), nil
}

func EvaluateExpression(expression string) (any, error) {
	logger.Debug("msg", "Evaluating an expression", "expression", expression)

	env := map[string]interface{}{
		"data": storage.Get().Items(),
		"env":  func(s string) string { return os.Getenv(s) },
	}

	return expr.Eval(expression, env)
}

func EvaluateExpressionAsBool(expression string) (bool, error) {
	var err error

	value, err := EvaluateExpression(expression)
	if err != nil {
		return false, err
	}

	switch t := value.(type) {
	default:
		return false, fmt.Errorf("can not cast result of expression '%s' of type %s as bool", expression, t)
	case bool:
		return value.(bool), nil
	case int:
		return value.(int) != 0, nil
	}
}
