# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.3.13] - 26.12.2024

- Dependencies massively updated

## [0.3.12] - 07.08.2024

### Fixed 

- `fetch_output` option to `exec` action handler

## [0.3.11] - 07.08.2024

### Added

- `fetch_output` option to `exec` action handler
- Yaml templating filters

## [0.3.10] - 11.07.2024

### Fixed

- Outdated dependencies

## [0.3.9] - 02.06.2024

### Added

- Support of RFC5424 and RFC 3164

### Fixed

- Syslog config templating

## [0.3.8] - 23.04.2024

### Added

- Filtering output buffering interval

## [0.3.7] - 05.04.2024

### Fixed

- Filtering output

## [0.3.6] - 25.03.2024

### Added

- Sprig v3
- Updated dependencies

## [0.3.5] - 14.03.2024

### Fixed

- Fetching output from child process

## [0.3.4] - 06.03.2024

### Added

- Localization
- Helm chart handler
- Refactor exec output fetching

## [0.3.3] - 13.02.2024

### Added

- More docs
- More configuration field template rendering

## [0.3.2] - 10.02.2024

### Added

- Documentation generator

## [0.3.1] - 06.02.2024

### Added

- Cross-platform compilation & CG_ENABLED

## [0.3.0] - 22.01.2024

### Added

- Expression language
- Thread-safe storage

## [0.2.5] - 15.01.2024

### Added

- Logger output settings
- Flow protocol support
- Syslog support

## [0.2.4] - 17.11.2022

### Added

- Added nc timeout parameter

## [0.2.3] - 18.10.2022

### Fixed

- Logger messages
